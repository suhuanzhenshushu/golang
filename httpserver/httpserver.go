package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"

	"github.com/gohutool/log4go"
	"math/rand"
)

var logger = log4go.LoggerManager.GetLogger("main")

func main() {

	logger.Info("http server...")
	myserver := HttpServer{"myserver"}
	myserver.handel("/healtz")
	myserver.init()
	myserver.start(":80")

}

type HttpServer struct {
	Name string
}

func (server *HttpServer) init() {
	logger.Info("http server...init")
}

func (server *HttpServer) start(port string) bool {
	logger.Info"http server...start")
	err := http.ListenAndServe(port, nil)
	if err != nil {
		log.Fatal(err)
		logger.Error(err)
	}
	return true
}

func (server *HttpServer) handel(path string) {
	http.HandleFunc(path, healtz)
	fmt.Println("http server...handel")
}

func healtz(w http.ResponseWriter, r *http.Request) {
	for key, val := range r.Header {
		fmt.Println("key:", key, "val :", val)
		w.Header().Set(key, val[0])
	}
	ip := RemoteIp(r)
	fmt.Println("request ip :", ip)

	logFile, _ := os.OpenFile("access.log", os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0644)
	log.SetOutput(logFile)
	log.Println("log ip: ", ip)

	ver, _ := os.LookupEnv("GOPATH")
	fmt.Println("ver:", ver)
	w.Header().Set("ver", ver)
	w.WriteHeader(200)
	io.WriteString(w, "200")
	fmt.Println("res code :", w.Header().Get("id"))
	fmt.Println("res code :", w.Header().Get("ver"))

	rand.Seed(time.Now().UnixNano())
	randomNum := rand.Intn(2)

	time.Sleep(randomNum*time.Second) 

}

func RemoteIp(req *http.Request) string {
	var remoteAddr string
	// RemoteAddr
	remoteAddr = req.RemoteAddr
	if remoteAddr != "" {
		return remoteAddr
	}
	// ipv4
	remoteAddr = req.Header.Get("ipv4")
	if remoteAddr != "" {
		return remoteAddr
	}
	//
	remoteAddr = req.Header.Get("XForwardedFor")
	if remoteAddr != "" {
		return remoteAddr
	}
	// X-Forwarded-For
	remoteAddr = req.Header.Get("X-Forwarded-For")
	if remoteAddr != "" {
		return remoteAddr
	}
	// X-Real-Ip
	remoteAddr = req.Header.Get("X-Real-Ip")
	if remoteAddr != "" {
		return remoteAddr
	} else {
		remoteAddr = "127.0.0.1"
	}
	return remoteAddr
}
